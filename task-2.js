// 1) Write a program that is divisible by 3 and 5

const divisibleCheck = (num) => {
  if (num % 3 === 0 && num % 5 === 0) {
    console.log(`${num} is divisible by 3 and 5`);
  } else {
    console.log(`${num} is not divisible by 3 and 5`);
  }
};

divisibleCheck(30);

// 2) Write a program that checks whether given number odd or even.

let num = 10;
if (num % 2 === 0) {
  console.log(`${num} is even number`);
} else {
  console.log(`${num} is odd number`);
}

// 3) Write a program that get array as argument and sorts it by order // [1,2,3,4...]

// let array = ['a','c','k','d','b'];
// console.log(array.sort());

let array = [1, 2, 5, 9, 20, 10];
console.log(array.sort((a, b) => a - b));

// 4) Write a program that return unique set of array:
// Input:
// [
// {test: ['a', 'b', 'c', 'd']},
// {test: ['a', 'b', 'c']},
// {test: ['a', 'd']},
// {test: ['a', 'b', 'k', 'e', 'e']},
// ]

// Output: [‘a’, ‘b’, ‘c’, ‘d’, ‘k’, ‘e’]

function uniqueArray(arg) {
  let arr = [];
  arg.map((item) => {
    for (let [key, value] of Object.entries(item)) {
      arr.push(...value);
    }
  });
  let newArr = arr.filter((val, pos) => {
    return arr.indexOf(val) === pos;
  });
  return newArr;
}

uniqueArray([
  {
    test: ["a", "b", "c", "d"],
  },
  {
    test: ["a", "b", "c"],
  },
  {
    test: ["a", "d"],
  },
  {
    test: ["a", "b", "k", "e", "e"],
  },
]);

// 5) Write a program that compares two objects by its values and keys, return true if object values are same otherwise return false

let obj1 = {
  name: "Farruh",
  surname: "Adhamov",
  age: 19,
};
let obj2 = {
  name: "Farruh",
  surname: "Adhamov",
  age: 19,
};

if (JSON.stringify(obj1) === JSON.stringify(obj2)) {
  console.log(`Same objects`);
} else {
  console.log(`Not the same objects`);
}
